dig
ps aux
grep
top
netstat -a | -tulpn
time
uptime
systemctl
screen :password
df -h
du -h
s3cmd
lsof
scp
rsync
pgrep | pkill
tar -zcvf <tarball> <dir>
tar -xzvf <tarball>
screen
sed
grep
top
openssl s_client -showcerts -connect www.example.com:443 </dev/null
find . -name "<file name>" -exec <command> {} \;

# SELinux
semanage fcontext -a -t <type> <file|dir> :  changes the default type for a file or dir (persists through reboots, directories will need regexp, all files need complete paths)
semanage fcontext -d <file|dir> :  deletes the set default type for file or dir
semanage fcontext -C -l :  will list set defaults
restorecon -v <file|dir> :  will restore the context of a file or dir to default
chcon -t <type> <file|dir> :  changes the type until reboot
matchpathcon -V <file|dir> :  checks whether or not files have the right default context
ausearch (just man this command)
audit2allow -M (generates te file, grep audit.log to be specific about which denial to add)
setsebool -P <bool_name> (-P persists through reboot)
ausearch -m avc -ts recent | audit2why   (shows why something was denied along with possible solutions)
