# Contains
This project contains all helpful snippets, lists, and other tools that I have found and want to keep track of

## General
* http://codecourse.sourceforge.net/materials/Code-Complete-A-Detailed-Book-Review.html
* https://blog.codinghorror.com/a-pragmatic-quick-reference/

## Programming
* https://github.com/sindresorhus/awesome
* https://github.com/LeCoupa/awesome-cheatsheets
* https://github.com/google/kubeflow
* https://github.com/mandliya/algorithms_and_data_structures
* https://github.com/ossu/computer-science
* https://github.com/kamranahmedse/developer-roadmap

## OPS
* https://github.com/binhnguyennus/awesome-scalability
* http://www.devopsbookmarks.com/
* https://github.com/cncf/landscape
* https://github.com/mspnp/architecture-center/blob/master/docs/index.md
* https://github.com/donnemartin/system-design-primer

## JS
* https://github.com/Chalarangelo/30-seconds-of-code
* https://github.com/AllThingsSmitty/must-watch-javascript
* https://github.com/getify/You-Dont-Know-JS

## Git
* https://github.com/k88hudson/git-flight-rules
* https://sethrobertson.github.io/GitFixUm/fixup.html
* https://git-scm.com/book/en/v2/Git-Tools-Advanced-Merging
* https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection#_triple_dot
* https://git-scm.com/book/en/v2/Git-Tools-Submodules
* https://git-scm.com/book/en/v2/Git-Internals-Git-Objects

## Bash
* https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md
* http://mywiki.wooledge.org/BashPitfalls

## OS
* https://github.com/tuhdo/os01
* https://github.com/s-matyukevich/raspberry-pi-os

## Kubernetes
* https://github.com/freach/kubernetes-security-best-practice
* https://itnext.io/kubernetes-hardening-d24bdf7adc25
* https://github.com/ramitsurana/awesome-kubernetes#main-resources
* https://medium.com/@noqcks/kubernetes-audit-logging-introduction-464a34a53f6c
* https://github.com/walidshaari/Kubernetes-Certified-Administrator
* https://github.com/gardener/gardener

## Docker
* https://github.com/jwilder/dockerize
* https://github.com/google/gvisor

## Hacking
* https://github.com/Hacker0x01/hacker101

## Interviewing
* https://github.com/yangshun/tech-interview-handbook
* https://github.com/jwasham/coding-interview-university
* https://github.com/yangshun/front-end-interview-handbook
* https://github.com/arialdomartini/Back-End-Developer-Interview-Questions
* https://github.com/kdn251/interviews
* https://www.fullstackinterviewing.com/2018/02/02/the-ultimate-guide-to-kicking-ass-on-take-home-coding-challenges.html

## Functional Programming
* https://mostly-adequate.gitbooks.io/mostly-adequate-guide/

## Databases
* https://github.com/LendingHome/zero_downtime_migrations
* https://github.com/github/gh-ost
* https://github.com/hasura/skor

## Deployment Tools
* https://deploystack.io

## UX
* https://github.com/krasimir/react-in-patterns
* https://medium.com/refactoring-ui/7-practical-tips-for-cheating-at-design-40c736799886
* https://atomiks.github.io/30-seconds-of-css/
* https://github.com/andrew--r/frontend-case-studies
* https://github.com/kieranmv95/Front-End-Wizard
* https://github.com/FrontendMasters/front-end-handbook-2018
* https://github.com/thedaviddias/Front-End-Design-Checklist
* https://github.com/AllThingsSmitty/css-protips

## HTML
* https://docs.emmet.io/cheat-sheet/

## Machine Learning
* https://github.com/kjaisingh/high-school-guide-to-machine-learning

## VIM
* https://www.digitalocean.com/community/tutorials/how-to-use-vim-for-advanced-editing-of-plain-text-or-code-on-a-vps--2

## Typing
* http://www.speedcoder.net/

## Regex
* https://regex101.com/

## OpenSSL
* https://www.sslshopper.com/article-most-common-openssl-commands.html

## Sed and Awk
* https://github.com/learnbyexample/Command-line-text-processing/blob/master/gnu_awk.md

## Cheatsheets
* https://docs.emmet.io/cheat-sheet/
* https://www.digitalocean.com/community/tutorials/how-to-use-vim-for-advanced-editing-of-plain-text-or-code-on-a-vps--2
* https://www.skorks.com/2009/09/bash-shortcuts-for-maximum-productivity/
* https://www.howtogeek.com/howto/44997/how-to-use-bash-history-to-improve-your-command-line-productivity/

## AI and ML
* https://modeldepot.io/

## GO
* https://github.com/enocom/gopher-reading-list


### Other
* https://github.com/tmcw/big
* https://kolide.com/fleet
