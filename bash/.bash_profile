export GITAWAREPROMPT=~/.bash/git-aware-prompt
source "${GITAWAREPROMPT}/main.sh"
source ~/.git-completion.bash
#export PS1="\W $ "
export PS1="\[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\] \W $ "
HISTIGNORE="[\ ]*:&:p *"
HISTTIMEFORMAT="%d/%m/%y %T "
function src { source ~/.bash_profile; }
function c { cmatrix -as -u 10; }
function cdl { cd $1; ls; }
function ll {
  if [ $# -eq 0 ]; then
    ls -larG
  else
    ls -larG $1
  fi; }
#alias lncnt="find . -name '$1' | xargs wc -l"
function sizes { du -sh * ; }
function lncnt { find . -name $1 | xargs wc -l; }
function wthr {
  if [ $# -eq 0 ]; then
    curl -4 http://wttr.in/boston;
  else
    curl -4 http://wttr.in/$1;
  fi; }
function p {
  if [ $# -eq 2 ]; then
    eval " `fc -ln -$1 -$2 `"
  else
    eval " `fc -ln -$1 -1 `";
  fi; }
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
